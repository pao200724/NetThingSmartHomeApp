/*
 * 版权：Copyright (c) 2019 红网
 * 
 * 创建日期：Tuesday April 23rd 2019
 * 创建者：胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 修改日期: Tuesday, 23rd April 2019 10:26:24 am
 * 修改者: 胡燕龙(huyl) - y.dragon.hu@hotmail.com
 * 
 * 说明
 * 		1、安联家居APP配置
 */
import { NetThingSmartHomeApplication } from './app';
import './mock';
import { AppMainFormControl } from 'src/pao-aop-buss/mainForm/appMainForm';
import { AppReactRouterControl, AjaxJsonRpcFactory, BlankMainFormControl } from 'pao-aop-client';
import { createObject } from 'pao-aop';
import { HomeViewControl } from './views/index';
import { IHomeService } from 'src/models/home';
import { LoginViewControl } from 'src/pao-aop-buss/views/login';
const remote = {
    url: 'http://localhost:3000/remoteCall',
    home: 'HomeService'
};

const path = {
    index: "/",
    home: "/home",
    detail: "/detail",
    device: "/device",
    circle: "/circle",
    user: "/user",
    scan: "/scan",
};

const mainFormID = 'main';

const mainForm = new AppMainFormControl(
    "安联家居",
    [
        { key: "home", title: "首页", icon: "antd@home", link: path.home },
        { key: "device", title: "设备", icon: "font@nt-device-list", link: path.device },
        { key: "circle", title: "圈子", icon: "font@nt-circle", link: path.circle },
        { key: "mine", title: "我的", icon: "antd@user", link: path.user }
    ],
    [
        { key: "scan", icon: "antd@scan", link: "/scan" }
    ]
);
// 空白窗体
const blank = new BlankMainFormControl();
const blankID = 'blank';
const router = new AppReactRouterControl(
    {
        [blankID]: blank,
        [mainFormID]: mainForm,
    },
    [
        {
            path: path.index,
            redirect: "home",
            exact: true
        },
        {
            path: path.home,
            mainFormID: mainFormID,
            targetObject: createObject(
                HomeViewControl, {
                    homeService_Fac: new AjaxJsonRpcFactory(IHomeService, remote.url, remote.home),
                })
        },
        {
            path: '/login',
            mainFormID: blankID,
            targetObject: createObject(LoginViewControl, {
                bottom_left: '用户注册'
            })
        },
    ]
);

export let defaultObject = new NetThingSmartHomeApplication(
    router
);

export default defaultObject;